" vim-plug {{{
call plug#begin('~/.local/share/nvim/plugged') " initialize vim-plug (plugin manager)

" appearance {{{
Plug 'lifepillar/vim-solarized8' " colours
Plug 'itchyny/lightline.vim' " vim-airline
Plug 'sheerun/vim-polyglot' " more syntax highlighting
Plug 'psliwka/vim-smoothie' " smooth scrolling
" }}}

" distraction-free {{{
Plug 'junegunn/goyo.vim' " distraction-free writing
Plug 'junegunn/limelight.vim' " highlight current line
" }}}

" features {{{
Plug 'godlygeek/tabular' " markdown
Plug 'plasticboy/vim-markdown' " markdown
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } " fzf
Plug 'junegunn/fzf.vim' " more fzf
" }}}

call plug#end() " end vim-plug block }}}

set shiftwidth=4 " set tab width
set softtabstop=4 " set tab width
set expandtab " expand tabs to spaces
set shiftround " round to nearest multiple of shiftwidth
set autoindent " auto indent lines to level of previous line
set termguicolors " use right colours
set number " line numbers
set cursorline " highlight current line
set foldmethod=marker " folding method
set conceallevel=2 " automatically conceal (some) markup (esp. markdown)
set inccommand=nosplit " preview :s
set wrap " automatically wrap text
set linebreak " only wrap at word boundaries

set background=dark " light background
colorscheme solarized8 " set colour scheme
let g:lightline = { 'colorscheme': 'solarized' } " lightline colour scheme

filetype plugin on " enable filetype plugins
let mapleader = " " " change leader key to SPC
let g:vim_markdown_folding_disabled = 1

" fzf file/buffer list (,p / ,f)
nnoremap <Leader>p :Buffers<CR>
nnoremap <Leader>f :Files<CR>

" goyo (distraction free writing) toggle (,-;)
nnoremap <Leader>; :Goyo<CR>

" move by visual line
nnoremap j gj
nnoremap k gk

" enable limelight (automatically dim lines) when entering goyo mode
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!
