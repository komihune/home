#!/bin/sh
echo '\nBacking up /media/komi/FILES...\n'
borg create --stats -v ~/Backups/FILES::$(date +%FT%R%:z) /media/komi/FILES && echo '\nBackup complete!'
